# GAFAM Firewall CLI
'GAFAM Firwall CLI' is Command Line Interface version of [GAFAM Firewall project](https://code.swecha.org/against-surveillance-capitalism/gafam-firewall)

### About GAFAM Firewall

### Installation
apt install gafam-firewall

### Contribute
Please check [CONTRIBUTE.md](/CONTRIBUTE.md) for more details
